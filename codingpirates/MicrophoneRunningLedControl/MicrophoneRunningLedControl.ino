#include <Adafruit_NeoPixel.h>

const bool DEBUG_TO_SERIAL = true;
const unsigned int OFFSET = 8; // used for packing the r g b values into a single unsigned int
const unsigned int MICROPHONE_PIN = 0;
const unsigned int PIXEL_PIN = 6;
const unsigned int NUMBER_OF_LEDS = 24;
const unsigned int SAMPLE_WINDOW = 100; // number of ms to listen for min and max values from microphone
const unsigned int DELAY_AFTER_SAMPLE = 100; // in order to slow down the leds moving to the sides, if we have a to small sample window

unsigned int leds[50]; // array holding all of the packed light values for each of leds. Must be larger than NUMBER_OF_LEDS
unsigned int middleLed=0; // will be calculated later based on NUMBER_OF_LEDS

// Parameter 1 = number of pixels in strip,  neopixel stick has 8
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip), correct for neopixel stick
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMBER_OF_LEDS, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.show();
  middleLed = NUMBER_OF_LEDS/2;
  for (int i=0; i < NUMBER_OF_LEDS; i++)
  {
    leds[i] = 0;
  }
  pinMode (MICROPHONE_PIN, INPUT) ;
  
  Serial.begin(9600); 
}

void loop() {
  int packedLightValue = convertSampleToPackedGraditedColor(sampleMic());
  insertColor(packedLightValue);
  
  tick();
  
  delay(DELAY_AFTER_SAMPLE);
}

unsigned int sampleMic()
{
  unsigned int sample;
  unsigned long startSampleMillis = millis();
  unsigned int peakToPeak = 0;

  unsigned int signalMax = 0;
  unsigned int signalMin = 1024;
  unsigned int freq = 0;

  while(millis() - startSampleMillis < SAMPLE_WINDOW)
  {
    sample = analogRead(MICROPHONE_PIN);
    //sample = digitalSample;
    if(sample < 1024)
    {
      if (sample > signalMax)
      {
        signalMax = sample;  // save just the max levels
      }
      else if (sample < signalMin)
      {
        signalMin = sample;  // save just the min levels
      }
    }
  }

  peakToPeak = (signalMax - signalMin) - 1;
  peakToPeak = peakToPeak <= 0 ? 0 : peakToPeak;

  if(DEBUG_TO_SERIAL == true)
  {
    Serial.print("peakToPeak ");
    Serial.print(peakToPeak);
  }

  return peakToPeak;
}

void tick()
{
  for (int i = 0; i < middleLed; i++)
  {
    leds[i] = leds[i+1];
    insertColorAt(i, leds[i]);
  }

  for (int i = NUMBER_OF_LEDS; i > middleLed; i--)
  {
    leds[i] = leds[i - 1];
    insertColorAt(i, leds[i]);
  }

  strip.show();
}
        
void insertColor(unsigned int color)
{
  leds[middleLed] = color;
  
  // insert code to call and update led with number middleLed with value from leds[middleLed]
  insertColorAt(middleLed, leds[middleLed]);
}

void insertColorAt(unsigned int led, unsigned int color)
{  
  unsigned int r = (color >> 0 * OFFSET) & ((1<<OFFSET)-1);
  unsigned int g = (color >> 1 * OFFSET) & ((1<<OFFSET)-1);
  unsigned int b = (color >> 2 * OFFSET) & ((1<<OFFSET)-1);

  if(DEBUG_TO_SERIAL == true && led == middleLed)
  {
    Serial.print(" -> ");
    Serial.print(r);
    Serial.print(" ");
    Serial.print(g);
    Serial.print(" ");
    Serial.print(b);
    Serial.println("");
  }
  
  // insert code to call and update led with number middleLed with value from leds[middleLed]
  strip.setPixelColor(led, r, g, b);
}

unsigned int convertSampleToPackedGraditedColor(unsigned int sample)
{
  unsigned int r, g, b;
  b = 0;
  if (sample > 254)
  {
    r = 254;
    g = 254;
  }
  else if (sample < 1)
  {
    r = 0;
    g = 0;
  }
  else if (sample < 128)
  {
    r = 254;
    g = (254*sample/128);
  }
  else
  {
    g = 254;
    r = 254 - (int) (254*(sample - 128)/128);
  }
  
  return (r << 0 * OFFSET) | (g << 1 * OFFSET) | (b << 2 * OFFSET);
}

